#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <pthread.h>

int const BUFFER_SIZE = 256;
int const BACKLOG_QUEUE_SIZE = 5;

void readQuery(int);

const char START_MSG[] = "start";
const char FINISH_MSG[] = "finish";
const char NOT_FIND_MSG[] = "not find";
const char FILE_FINDED_MSG[] = "finded";

void (*add_client)(int sock);

void error(const char *msg)
{
    fprintf(stderr, "%s\n", msg);
    exit(EXIT_FAILURE);
}

void sendNotFindMsg(int socket)
{
    int writed = write(socket, NOT_FIND_MSG, sizeof(NOT_FIND_MSG));
    if(writed < 0) {
        error("Ошибка записи NOT_FIND сообщения");
    }
}

void sendStartMsg(int socket)
{
    int writed = write(socket, START_MSG, sizeof(START_MSG));
    if(writed < 0){
        error("Ошибка записи START сообщения");
    }
}

void sendFileFindedMsg(int socket)
{
    int writed = write(socket, FILE_FINDED_MSG, sizeof(FILE_FINDED_MSG));
    if(writed < 0){
        error("Ошибка записи FILE_FINDED сообщения");
    }
}


void sendFinishMsg(int socket)
{
    int writed = write(socket, FINISH_MSG, sizeof(FINISH_MSG));
    if(writed < 0){
        error("Ошибка записи FINISH сообщения");
    }
}

void loadFile(int socket, int fd){
    char buffer[BUFFER_SIZE];
    ssize_t readed, writed, read_from_socket;
    bzero(buffer, BUFFER_SIZE);

    char message[BUFFER_SIZE];

    while((readed = read(fd, buffer, BUFFER_SIZE)) > 0) {
        bzero(message, BUFFER_SIZE);
        read_from_socket = read(socket, message, BUFFER_SIZE);
        if(read_from_socket < 0){
            error("Ошибка чтения сокета");
        }

        if(strcmp(message, START_MSG) == 0){
            writed = write(socket, buffer, (ssize_t) readed);

            if(writed < 0){
                error("Ошибка записи в сокет");
            }

            if(writed != readed)
                error("Ошибка записи в сокет");
        }
        else{
            error("Ошибка записи в сокет");
        }
    }

    read_from_socket = read(socket, message, BUFFER_SIZE);
    if(read_from_socket < 0){
        error("Ошибка чтения сокета");
    }
    if(strcmp(message, START_MSG) == 0){
        sendFinishMsg(socket);
    }
    else{
        error("Ошибка записи в сокет");
    }
}

void prepareToDownloadFile(int socket)
{
    int n;
    char buffer[BUFFER_SIZE];
    bzero(buffer, BUFFER_SIZE);

    n = read(socket, buffer, BUFFER_SIZE);
    if(n < 0)
        error("Ошибка при чтении из сокета");

    printf("Готовлюсь передать файл %s\n", buffer);

    int fd = open(buffer, O_RDONLY);

    if(fd == -1) {
        sendNotFindMsg(socket);
        error("Ошибка при открытии файла");
    }
    else {
        sendFileFindedMsg(socket);

        loadFile(socket, fd);


        printf("%s\n", "Файл передан");
    }

    close(fd);
}

void add_client_as_process(int sock){
    switch(fork()){
    case -1:
           error("Ошибка при создании процесса");
           break;
    case 0:
          prepareToDownloadFile(sock);
          exit(0);
          break;

    default:
           close(sock);
    }
}

void* start_thread(void* arg){
    int sock = * (int*) arg;
    prepareToDownloadFile(sock);
}

void add_client_as_thread(int sock){
    pthread_t pd;
    if(pthread_create(&pd, NULL, start_thread, &sock) < 0) {
        error("Ошибка создания потока");
    }
}

void open_socket(int port)
{
    int sockfd, newsockfd;
    socklen_t clilen;
    struct sockaddr_in serv_addr, cli_addr;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("Ошибка открытия сокета");
    else{
        printf("%s %d\n", "открыл порт номер", port);
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(port);

    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        error("Ошибка при биндинге");

    listen(sockfd, 5);

    clilen = sizeof(cli_addr);

    while (1) {
        newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        if (newsockfd < 0)
             error("ERROR on accept");

        add_client(newsockfd);
     }

     close(sockfd);
}

int main(int argc, char *argv[])
{
#ifdef tf
    add_client = add_client_as_thread;
    printf("Работа с потоками\n");
#else
    add_client = add_client_as_process;
    printf("Работа с процессами\n");
#endif

    if (argc < 2) {
        error("Передайте номера портов как аргументы коммандной строки");
    }
    else {
        open_socket(atoi(argv[1]));
    }

    return 0;
}

