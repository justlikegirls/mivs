#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>

const int BUFFER_SIZE = 256;

const char START_MSG[] = "start";
const char FINISH_MSG[] = "finish";
const char NOT_FIND_MSG[] = "not find";
const char FILE_FINDED_MSG[] = "finded";

void error(const char *msg)
{
    fprintf(stderr, "%s\n", msg);
    exit(EXIT_FAILURE);
}


void send_start_message(int sockfd)
{
    int writed = write(sockfd, START_MSG, sizeof(START_MSG));
    if(writed < 0){
        printf("Ошибка записи в сокет");
    }
}

void download_file_from_server(int sock, char* file_name)
{
    int readed, writed;
    char buffer[BUFFER_SIZE];

    int fd = open(file_name, O_RDWR | O_CREAT);
    if(fd < 0)
        error("Ошибка создания копии файла");

    while(1){
        bzero(buffer, BUFFER_SIZE);
        send_start_message(sock);
        readed = read(sock, buffer, BUFFER_SIZE);

        if (readed < 0)
             error("Ошибка чтения из сокета");

        if(strcmp(buffer, FINISH_MSG) == 0)
            break;

        writed = write(fd, buffer, readed);
        if(readed != writed){
            error("Ошибка записи в файл");
        }
    }

    close(fd);
}

void gets_s(char *buffer, int buffer_size)
{
    fgets(buffer, buffer_size, stdin);
    int length = strlen(buffer);
    if(buffer[length - 1] == '\n')
        buffer[length - 1] = 0;
}

void download_file(char *hostname, int port)
{
    int sockfd, readed, writed;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buffer[BUFFER_SIZE];
    char file_name[BUFFER_SIZE];

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("Ошибка открытия сокета");

    server = gethostbyname(hostname);
    if (server == NULL) {
        error("Неправильный домен");
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);
    bcopy((char *)server->h_addr_list[0], (char *)&serv_addr.sin_addr.s_addr,
          server->h_length);

    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
        error("Ошибка подключения");

    printf("Введите имя файла: ");
    bzero(file_name, BUFFER_SIZE);
    gets_s(file_name, BUFFER_SIZE - 1);

    writed = write(sockfd, file_name, strlen(file_name));
    if (writed < 0)
         error("Ошибка при записи в сокет");

    readed = read(sockfd, buffer, BUFFER_SIZE - 1);
    if(readed < 0) {
        error("Ошибка при чтении из сокета");
    }
    else {
        if(strcmp(buffer, FILE_FINDED_MSG) == 0){
            download_file_from_server(sockfd, file_name);
            printf("Файл загружен\n");
        }
        else if(strcmp(buffer, NOT_FIND_MSG) == 0){
            error("Файл не найден");
        }
        else
            error("Непредвиденный сигнал от сервера");
    }

    close(sockfd);
}

int main(int argc, char *argv[])
{

    if (argc < 3) {
       error("Введите домен и номер порта в качестве аргментов");
    }

    download_file(argv[1], atoi(argv[2]));

    return 0;
}
