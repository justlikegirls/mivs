source config.ini

insmod main.ko || exit

rm -f /dev/"$input_name"
rm -f /dev/"$output_name"

mknod /dev/"$input_name" c $(awk -v var=$input_name '$2==var {print $1}' /proc/devices) 0
mknod /dev/"$output_name" c $(awk -v var=$output_name '$2==var {print $1}' /proc/devices) 0

chmod 666 /dev/"$input_name"
chmod 444 /dev/"$output_name"
