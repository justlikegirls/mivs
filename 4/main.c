#include <linux/fs.h> 
#include <linux/init.h> 
#include <linux/module.h> 
#include <asm/uaccess.h> 
#include <linux/kernel.h>
#include <linux/string.h>

MODULE_LICENSE( "GPL" ); 
MODULE_AUTHOR( "Gavrilovich R.M" ); 

static int main_init( void );
static void main_exit( void );
static int input_device_open(struct inode *, struct file *);
static int output_device_open(struct inode *, struct file *);
static int input_device_release(struct inode *, struct file *);
static int output_device_release(struct inode*, struct file *);
static ssize_t input_device_write(struct file *, const char *, size_t, loff_t *);
static ssize_t output_device_read(struct file *, char *, size_t, loff_t *);

#define SUCCESS 0
#define BUF_LEN 256
#define INPUT_DEVICE_NAME "F_input_device"
#define OUTPUT_DEVICE_NAME "F_output_device"

static int input_major = -1;
static int is_input_opened = 0;

static int output_major = -1;
static int is_output_opened = 0;

static char msg[BUF_LEN] = { 0 };
static int buffer_current_size = sizeof(msg);

static int value_updated = 0;

static struct file_operations input_device_fops = {
	.write = input_device_write,
	.open = input_device_open,
	.release = input_device_release
};

static struct file_operations output_device_fops = {
	.read = output_device_read,
	.open = output_device_open,
	.release = output_device_release
};

static int main_init(void)
{
	output_major = register_chrdev(0, OUTPUT_DEVICE_NAME, &output_device_fops);
	if(output_major < 0) {
		printk(KERN_ALERT "=== Registering output char device failed with %d\n", output_major);
	}
	else {
		printk(KERN_INFO "=== Registering output char device success with major: %d\n", output_major);
	}
	
	input_major = register_chrdev(0, INPUT_DEVICE_NAME, &input_device_fops);
	if(input_major < 0) {
		printk(KERN_ALERT "=== Registrering input char device failed with %d\n", input_major);
		return input_major;
	}
	else {
		printk(KERN_INFO "=== Registering input char device success with major: %d\n", input_major);
	}
	
	return SUCCESS;
}

static void main_exit(void)
{
	if(input_major > 0) {
		unregister_chrdev(input_major, INPUT_DEVICE_NAME);
		printk(KERN_INFO "== Input char device success closed\n");
	}
	
	if(output_major > 0) {
		unregister_chrdev(output_major, OUTPUT_DEVICE_NAME);
		printk(KERN_INFO "== Output char device success closed\n");
	}
}

static int input_device_open(struct inode *ind, struct file *f)
{
	if(is_input_opened)
		return -EBUSY;
		
	is_input_opened = 1;
	printk(KERN_INFO "== Input device opened\n");
	
	return SUCCESS;
}

static int output_device_open(struct inode *ind, struct file *f)
{
	if(is_output_opened)
		return -EBUSY;
		
	is_output_opened = 1;
	printk(KERN_INFO "== Output device opened\n");
	
	return SUCCESS;
}


static int input_device_release(struct inode *ind, struct file* f)
{
	is_input_opened = 0;
	
	return SUCCESS;
}

static int output_device_release(struct inode *ind, struct file *f)
{
	is_output_opened = 0;
	
	return SUCCESS;
}

static int get_fib_number(long n)
{
	long fib_number = -1;
	if(n == 0) {
		fib_number = 0;
	}
	else if(n == 1) {
		fib_number = 1;
	}
	else {
		long prev_number = 1;
		long prev2_number = 0;
		long i = 2;
		while(i <= n){
			fib_number = prev2_number + prev_number;
			prev2_number = prev_number;
			prev_number = fib_number;
			i++;
		}
	}
	
	return fib_number;
}

static ssize_t input_device_write(struct file *flip, 
								  const char *buffer, 
								  size_t length, 
								  loff_t *offset)
{
	long n = 0;
	long fib_number = 0;
	
	if(copy_from_user(msg, buffer, length)) {
		printk(KERN_ALERT "Error on write");
		return -EFAULT;
	}

	kstrtol(msg, BUF_LEN, &n);
	printk("N is %ld\n", n);
	
	fib_number = get_fib_number(n);
	if(fib_number < 0) {
		strncpy(msg, "Incorrect N", BUF_LEN);
	}
	else {
		printk("Fib number is %ld\n", fib_number);
		snprintf(msg, BUF_LEN, "%ld\n", fib_number);
	}
	
	value_updated = 1;
	
	return length;
}

static ssize_t output_device_read(struct file *filp, 
								  char *buffer, 
								  size_t length, 
								  loff_t * offset)
{
	if(!value_updated)
		return 0;
	
	value_updated = 0;
		
	if(buffer_current_size > length) {
		printk(KERN_ALERT "Not enoght space in buffer\n");
		return -EFAULT;
	}
	
	if(copy_to_user(buffer, msg, buffer_current_size)) {
		printk(KERN_ALERT "Reading failed\n");
		return -EFAULT;
	}
	
	return buffer_current_size;
}


module_init(main_init);
module_exit(main_exit);
