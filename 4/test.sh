source config.ini
bash init.sh

current_number=0
fib_expected=0
echo Current N: "$current_number"
echo $current_number > /dev/"$input_name"
fib_number=$(cat /dev/"$output_name")
echo Current Fib. number: "$fib_number"
if [ "$fib_number"="$fib_expected" ]
then echo "Test passed"
else echo "Test failed"
fi

current_number=1
fib_expected=1
echo Current N: "$current_number"
echo $current_number > /dev/"$input_name"
fib_number=$(cat /dev/"$output_name")
echo Current Fib. number: "$fib_number"
if [ "$fib_number"="$fib_expected" ]
then echo "Test passed"
else echo "Test failed"
fi

current_number=5
fib_expected=5
echo Current N: "$current_number"
echo $current_number > /dev/"$input_name"
fib_number=$(cat /dev/"$output_name")
echo Current Fib. number: "$fib_number"
if [ "$fib_number"="$fib_expected" ]
then echo "Test passed"
else echo "Test failed"
fi

current_number=-1
fib_expected="Incorrect N"
echo Current N: "$current_number"
echo $current_number > /dev/"$input_name"
fib_number=$(cat /dev/"$output_name")
echo Current Fib. number: "$fib_number"
if [ "$fib_number"="$fib_expected" ]
then echo "Test passed"
else echo "Test failed"
fi

bash exit.sh
